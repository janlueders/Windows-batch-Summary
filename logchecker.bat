@echo off
setlocal

set "file=logfile.txt"

:: 300 minutes == 5 Hours
set "execTime=300"

:: get full path to file
for %%F in ("%file%") do set "file=%%~fF"

:: escape the backslashes
set "file=%file:\=\\%"

:: get the last modified timestamp for the file
set "fileTime="
for /f "skip=1" %%A in (
  'wmic datafile where "name='%file%'" get lastModified'
) do if not defined fileTime set "fileTime=%%A"

:: convert file timestamp to unix time format
call :UnixTime fileTime %fileTime%

:: get current timestamp in unix time format
call :Unixtime currentTime

:: compute interval in seconds, and convert execTime into seconds
set /a "diff=currentTime-fileTime, execSec=execTime*60"

:: what should we do?
if %diff% gtr %execSec% (
  powershell 
) else (
  exit 0
)

:: Must exit so we don't fall into the function below
exit /b


:UnixTime  [ReturnVar]  [TimeStamp]

setlocal
set "ts=%~2"
if not defined ts for /f "skip=1 delims=" %%A in ('wmic os get localdatetime') do if not defined ts set "ts=%%A"
set /a "yy=10000%ts:~0,4% %% 10000, mm=100%ts:~4,2% %% 100, dd=100%ts:~6,2% %% 100"
set /a "dd=dd-2472663+1461*(yy+4800+(mm-14)/12)/4+367*(mm-2-(mm-14)/12*12)/12-3*((yy+4900+(mm-14)/12)/100)/4"
set /a ss=(((1%ts:~8,2%*60)+1%ts:~10,2%)*60)+1%ts:~12,2%-366100-%ts:~21,1%((1%ts:~22,3%*60)-60000)
set /a ss+=dd*86400
endlocal & if "%~1" neq "" (set %~1=%ss%) else echo %ss%
exit /b
